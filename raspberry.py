# This module used for read the temperature and humidity values from
# a DHT11 and a DHT22 sensors.
# You can learn the basics from this sensors in here: https://learn.adafruit.com/dht
#
# After reading the sensors data it store the data to a database and
# send the data to thingsboar.io to visualiza this data.
# For sending data this module uses MQTT protocol and parse the data to JSON format
#
# Storing data, a lightweight, sqlite database is used via sqlite3 package
# This can be easily replaced with any SQL database
#
# Dependecies:
#   - Adafruit_DHT
#   - Paho MQTT  
#   - sqlite3
# (You can install these packageses to you python interpreter using PIP)


# This module has a main so it can be called by any python interpreter

# imports

import os
import time
import datetime
import sys
import Adafruit_DHT as dht # https://github.com/adafruit/Adafruit_Python_DHT
import paho.mqtt.client as mqtt # https://pypi.org/project/paho-mqtt/
import json # https://docs.python.org/3/library/json.html
import sqlite3

def main():
    status = 0
    host = 'demo.thingsboard.io' # Host address
    access_token = 'Raspberry-DHT22' # Access token to authentication
    port = 1883
    keepalive_sec = 60
    interval = 10 # Data capture and upload interval in seconds.

    next_reading = time.time() 

    client = mqtt.Client()

    # Set access token
    client.username_pw_set(access_token)

    # Connect to ThingsBoard
    client.connect(host, port, keepalive_sec)

    client.loop_start()

    # Connect to local db

    db_con = connectToDb("/home/pi/raspberry_dht")

    try:
        while True:
            first_sensor = readDataFromSensor(dht.DHT11, 22)
            second_sensor = readDataFromSensor(dht.DHT22, 4)

            temperature_difference = abs(first_sensor[str(dht.DHT11) + '_temperature'] - second_sensor[str(dht.DHT22) + '_temperature'])
            humidity_difference = abs(first_sensor[str(dht.DHT11) + '_humidity'] - second_sensor[str(dht.DHT22) + '_humidity'])

            print("Differences:\tTemperature: " + str(temperature_difference) + "C\t" + "Humidity: " + str(humidity_difference))

            differences = {"temp_dif": temperature_difference, "hum_dif": humidity_difference}

            publishToThingsBoard(json.dumps(first_sensor), client)
            publishToThingsBoard(json.dumps(second_sensor), client)
            publishToThingsBoard(json.dumps(differences), client) # This is only done here because it's eaiser to calc the differences in python than in thingsboard

            writeToDb(db_con, str(dht.DHT11) + '_temperature', first_sensor[str(dht.DHT11) + '_temperature'])
            writeToDb(db_con, str(dht.DHT22) + '_temperature', second_sensor[str(dht.DHT22) + '_temperature'])
            writeToDb(db_con, str(dht.DHT11) + '_humidity', first_sensor[str(dht.DHT11) + '_humidity'])
            writeToDb(db_con, str(dht.DHT22) + '_humidity', second_sensor[str(dht.DHT22) + '_humidity'])

            waitForNextRead(interval, next_reading)
    except KeyboardInterrupt:
        pass
    client.loop_stop()
    client.disconnect()
    db_con.close()

    return status

def readDataFromSensor(sensorType, pinNumber):
    sensor_data = {str(sensorType) + '_temperature': 0, str(sensorType) + '_humidity': 0 }
    humidity,temperature = dht.read_retry(sensorType, pinNumber)
    humidity = round(humidity, 2)
    temperature = round(temperature, 2)

    print("Values from: " + str(sensorType)  + "    Temperature: " + str(temperature) + "C Humidity: " + str(humidity) + " %") 

    sensor_data[str(sensorType) + '_temperature'] = temperature
    sensor_data[str(sensorType) + '_humidity'] = humidity

    return sensor_data

def connectToDb(dbDir):
    print("Connecting to database...")

    db_file_path = os.path.join(dbDir, 'sensorBackup.db')

    conn = sqlite3.connect(db_file_path)
    print("Connection done")

    return conn


def writeToDb(conn, key, value):
    c = conn.cursor()

    last_id = c.execute("SELECT MAX(ID) FROM SensorData")
    last_id = last_id.fetchone()
    last_id = str(last_id)

    if('None' in last_id):
        last_id = str(0)

    last_id = last_id.replace('(', '')
    last_id = last_id.replace(')', '')
    last_id = last_id.replace(',', '')

    last_id = int(last_id)
    last_id = last_id + 1
    last_id = str(last_id)

    current_date = str(datetime.datetime.now())
    current_date = current_date.replace(' ', '_')

    c.execute("INSERT INTO SensorData " + "VALUES (" + last_id + ",'" + str(key) + "','" + str(value) + "','" + current_date + "')")
    
    conn.commit()

def publishToThingsBoard(json, client):
    client.publish('v1/devices/me/telemetry', json, 1)

def waitForNextRead(interval, nextReading):
    nextReading += interval
    sleep_time = nextReading-time.time()
    if sleep_time > 0:
        time.sleep(sleep_time)

status = main()
exit(status)

#end
